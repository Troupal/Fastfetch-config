<!-- <style>
.centered {
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center; /* Centrer le contenu horizontalement */
  position: relative; /* Position relative pour le centrage absolu */
}

.centered-links {
  display: flex;
  justify-content: center;
  align-items: center;
  white-space: nowrap; /* Empêche le lien de passer à la ligne */
  position: absolute;
}

.point_milieu {
  white-space: nowrap; /* Empêche le lien de passer à la ligne */
  position: absolute;
}

\0\<!-- .point_center{
  \<\!-- content: "•"; --\>
  white-space: nowrap; /* Empêche le lien de passer à la ligne */
  position: absolute;
  left: 50%;
  <!-- transform: translateX(-50%); --\>
  margin: 0 5px; /* Ajustez la marge pour positionner le séparateur */
} --\>

.centered-links *{
  white-space: nowrap; /* Empêche le lien de passer à la ligne */
  position: absolute;
}

.centered-links:nth-of-type(1) {
  right: calc(50% + 45px); /* Ajustez la valeur pour décaler le premier lien */
}

#point_tout-court_1 {
  right: calc(50% + 35px); /* Ajustez la valeur pour décaler le deuxième lien */
}

.centered-links:nth-of-type(2) {
  left: calc(50% - 31px); /* Ajustez la valeur pour décaler le deuxième lien */
}

#point_tout-court_2 {
  left: calc(50% + 35px); /* Ajustez la valeur pour décaler le deuxième lien */
}

.centered-links:nth-of-type(3) {
  left: calc(50% + 45px);
}

img:first-of-type {
  \<!-- content: "\A\A\A";; /* Ajoute un saut de ligne */ --\>
  white-space: pre; /* Garde le saut de ligne */
  display: inline; /* Affiche le saut de ligne comme un bloc */
  padding-top: 2.5rem;
}

</style>

# Fastfetch Config {.centered}

#### Mes fichiers de configuration Fastfetch {.centered}

[À propos](#à-propos){.centered-links #link_1} <span class=point_milieu id=point_tout-court_1>•</span> [Utilisation](#utilisation){.centered-links #link_2} <span class=point_milieu id=point_tout-court_2>•</span> [Licence](#licence){.centered-links #link_3}

![alt text](image.png) -->
<div align="center">

  <h1>
    Fastfetch Config
  </h1>

  <h4>Mes fichiers de configuration Fastfetch</h4>
  
  <p>
    <a href="#à-propos">À propos</a>&ensp;•&ensp;
    <a href="#utilisation">Utilisation</a>&ensp;•&ensp;
    <a href="#licence">Licence</a>
    <br><br>
    <img src="image.png" alt="Capture d'écran">
  </p>

</div>

## À propos

Dépôt contenant mes configurations pour Fastfetch afin de pouvoir les utiliser sur d'autres périphériques.

## Utilisation

1. ``git clone https://gitlab.com/Troupal/Fastfetch-config``
2. Renommer le dossier ``fastfetch-config`` en ``fastfetch``
3. Coller le dossier dans ``~/.local/share``

## Licence

GNU GPLv3
